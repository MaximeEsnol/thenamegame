const answers = ["unisex", "male", "male", "female", "male", "male", "unisex", "male"];

/*
* Loops through all 'keuze' elements, and then again through all input children.
* It will verify if the selected value matches the one in the answers array.
*/
function startVerification(){
      let elems = document.getElementsByClassName('keuze');

      let userAnswers = [];

      for(let index = 0; index < elems.length; index++){

            let choices = document.getElementsByName('gender-' + index);

            for(let x = 0; x < choices.length; x++){
                  let choice = choices[x];

                  if(choice.checked){
                        userAnswers.push(choice.value);
                  } 
            }
      }

      let wrongAnswers = highlightWrongAnswers(userAnswers);

      // alert("You guessed " + (answers.length - wrongAnswers) + " out of " + answers.length + " correctly!");
}

function showCard(id){  
      let elems = document.getElementsByTagName('section');

      if(id > 0 && id < elems.length){
            elems[id-1].setAttribute('class', 'exitCard');

            setTimeout(function(){
                  elems[id].setAttribute('class', 'introCard');
                  elems[id-1].style.display = "none";
            }, 250);

            setTimeout(function(){
                  elems[id].style.display = "flex";
            }, 250);

      } else if(id == 0){
            elems[id].setAttribute('class', 'introCard');
            setTimeout(function(){
                  elems[id].style.display = "flex";
            }, 250);
      } else if(id == elems.length){
            elems[id - 1].setAttribute('class', 'exitCard');

            setTimeout(function(){
                  startVerification();
                  elems[id -1].style.display = "none";
            })
      }
}

/*
* Highlights the answers that don't match the ones in the answers array with a 
* red background color. It also returns the number of wrong answers.
*/
function highlightWrongAnswers(userAnswers){
      let elems = document.getElementsByTagName('section');
      let wrongAnswers = 0;

      let goodAnswers = document.createElement('div');
      goodAnswers.setAttribute('class', 'correct');

      let h2Good = document.createElement('h2');
      h2Good.appendChild(document.createTextNode('Good answers'));
      goodAnswers.appendChild(h2Good);

      let badAnswers = document.createElement('div');
      badAnswers.setAttribute('class', 'wrong');

      let h2Bad = document.createElement('h2');
      h2Bad.appendChild(document.createTextNode('Wrong answers'));
      badAnswers.appendChild(h2Bad);
      
      let parent = document.getElementById('content');
      parent.appendChild(goodAnswers);
      parent.appendChild(badAnswers);

      for(let i = 0; i < answers.length; i++){
            if(userAnswers[i] != answers[i]){
                  wrongAnswers++;

                  let badDiv = document.createElement('div');
                  badDiv.appendChild(
                        document.createTextNode(
                              document.getElementsByTagName('section')[i]
                              .getElementsByClassName('naam')[0]
                              .innerHTML
                        )
                  );

                  badAnswers.appendChild(badDiv);
            } else {
                  let goodDiv = document.createElement('div');
                  goodDiv.appendChild(
                        document.createTextNode(
                              document.getElementsByTagName('section')[i]
                              .getElementsByClassName('naam')[0]
                              .innerHTML
                        )
                  );

                  goodAnswers.appendChild(goodDiv);
            }
      }

      
      let h1 = "<h2>"  + (answers.length - wrongAnswers) + "/" + answers.length + " correct!" + "</h2>";

      parent.parentElement.insertAdjacentHTML('afterbegin', h1);

      parent.parentElement.setAttribute('class', "answers introCard");

      setTimeout(function(){
            parent.parentElement.style.display = "flex";
      }, 250);


      return wrongAnswers;
}

/*
* Loops through all 'keuze' elements and creates radio inputs for each of them.
*/
function initialiseInputs(){
      let elems = document.getElementsByClassName('keuze');

      for(let index = 0; index < elems.length; index++){
            createChoiceElements(elems[index], index);
      }

      setTimeout(function(){
            showCard(0);
      }, 4500);
     
}

/*
* Creates radio button groups for the provided element. It will regroup them
* using the given index.
*/
function createChoiceElements(elem, index){
      let divMale = document.createElement('div');

      let inputMale = document.createElement('input');
      inputMale.setAttribute('type', 'radio');
      inputMale.setAttribute('name', 'gender-' + index);
      inputMale.setAttribute('value', 'male');
      inputMale.setAttribute('id', 'male-' + index);
      inputMale.setAttribute('onclick', 'showCard(' + (index + 1) + ')');

      let labelMale = document.createElement('label');
      labelMale.setAttribute('for', 'male-' + index);

      labelMale.appendChild(
            document.createTextNode('Male')
      );

      divMale.appendChild(inputMale);
      divMale.appendChild(labelMale);


      let divFemale = document.createElement('div');
      
      let inputFemale = document.createElement('input');
      inputFemale.setAttribute('type', 'radio');
      inputFemale.setAttribute('name', 'gender-' + index);
      inputFemale.setAttribute('value', 'female');
      inputFemale.setAttribute('id', 'female-' + index);
      inputFemale.setAttribute('onclick', 'showCard(' + (index + 1) + ')');

      let labelFemale = document.createElement('label');
      labelFemale.setAttribute('for', 'female-' + index);

      labelFemale.appendChild(
            document.createTextNode('Female')
      );

      divFemale.appendChild(inputFemale);
      divFemale.appendChild(labelFemale);


      let divUni = document.createElement('div');
      
      let inputUni = document.createElement('input');
      inputUni.setAttribute('type', 'radio');
      inputUni.setAttribute('name', 'gender-' + index);
      inputUni.setAttribute('value', 'unisex');
      inputUni.setAttribute('id', 'uni-' + index);
      inputUni.setAttribute('onclick', 'showCard(' + (index + 1) + ')');

      let labelUni = document.createElement('label');
      labelUni.setAttribute('for', 'uni-' + index);

      labelUni.appendChild(
            document.createTextNode('Unisex')
      );

      divUni.appendChild(inputUni);
      divUni.appendChild(labelUni);

     elem.appendChild(divMale);
     elem.appendChild(divFemale);
     elem.appendChild(divUni);
}

window.onload = function(){
      this.initialiseInputs();
}